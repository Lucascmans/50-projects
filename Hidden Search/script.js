const search = document.querySelector('.search');
const btn = document.getElementById('search-btn');
const input = document.getElementById('search-input');


btn.addEventListener('click', () => {
        search.classList.toggle('active');
        input.focus();
});